import org.junit.Assert;
import org.junit.Test;
import java.io.PrintStream;
import java.util.Scanner;

public class GameITTest {

    @Test
    public void shouldReturnScoreForConsolePlayersAfterEndOfTheGame(){
        PlayerBehavior playerBehavior = new ConsolePlayerBehavior(new Scanner("1 \n 2 \n 1"));
        Player player1 = new Player(playerBehavior);
        PlayerBehavior playerBehavior2 = new ConsolePlayerBehavior(new Scanner("1 \n 2 \n 1"));
        Player player2 = new Player(playerBehavior2);
        Machine machine = new Machine();
        PrintStream printStream = new PrintStream(System.out);
        Game game = new Game(player1,player2,machine,printStream);
        game.play(3);
        Assert.assertEquals(4,player1.getTotalScore());
        Assert.assertEquals(4,player2.getTotalScore());
    }

    @Test
    public void shouldReturnScoreForConsoleAndCoolPlayersAfterEndOfTheGame(){
        PlayerBehavior playerBehavior = new ConsolePlayerBehavior(new Scanner("1 \n 2 \n 1"));
        Player player1 = new Player(playerBehavior);
        PlayerBehavior playerBehavior2 = new CoolPlayerBehavior();
        Player player2 = new Player(playerBehavior2);
        Machine machine = new Machine();
        PrintStream printStream = new PrintStream(System.out);
        Game game = new Game(player1,player2,machine,printStream);
        game.play(3);
        Assert.assertEquals(7,player1.getTotalScore());
        Assert.assertEquals(3,player2.getTotalScore());
    }

    @Test
    public void shouldReturnScoreForCheatAndCoolPlayersAfterEndOfTheGame(){
        PlayerBehavior playerBehavior = new CheatPlayerBehviour();
        Player player1 = new Player(playerBehavior);
        PlayerBehavior playerBehavior2 = new CoolPlayerBehavior();
        Player player2 = new Player(playerBehavior2);
        Machine machine = new Machine();
        PrintStream printStream = new PrintStream(System.out);
        Game game = new Game(player1,player2,machine,printStream);
        game.play(3);
        Assert.assertEquals(9,player1.getTotalScore());
        Assert.assertEquals(-3,player2.getTotalScore());
    }

    @Test
    public void shouldReturnScoreWithCopyCatPlayer(){
        PlayerBehavior playerBehavior = new ConsolePlayerBehavior(new Scanner("1 \n 2 \n 1"));
        Player player1 = new Player(playerBehavior);
        CopyCatPlayerBehavior playerBehavior2 = new CopyCatPlayerBehavior();
        Player player2 = new Player(playerBehavior2);
        Machine machine = new Machine();
        PrintStream printStream = new PrintStream(System.out);
        Game game = new Game(player1,player2,machine,printStream);
        game.addObserver(playerBehavior2);
        game.play(3);
        Assert.assertEquals(4,player1.getTotalScore());
        Assert.assertEquals(4,player2.getTotalScore());
    }

}

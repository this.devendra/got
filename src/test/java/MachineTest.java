import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.HashMap;

public class MachineTest {

    Machine machine = new Machine();

    @Test
    public void shouldReturnScoreWhenBothPlayersCooperate(){
        Score score = machine.getScore(MoveType.COOPERATE, MoveType.COOPERATE);
        Assert.assertEquals(2,score.getPlayer1Score());
        Assert.assertEquals(2,score.getPlayer2Score());
    }

    @Test
    public void shouldReturnScoreWhenPlayer1CheatAndPlayer2Coperate(){
        Score score = machine.getScore(MoveType.CHEAT, MoveType.COOPERATE);
        Assert.assertEquals(3,score.getPlayer1Score());
        Assert.assertEquals(-1,score.getPlayer2Score());
    }

    @Test
    public void shouldReturnScoreWhenPlayer1CooperateAndPlayer2Cheat(){
        Score score = machine.getScore(MoveType.COOPERATE, MoveType.CHEAT);
        Assert.assertEquals(-1,score.getPlayer1Score());
        Assert.assertEquals(3,score.getPlayer2Score());
    }

    @Test
    public void shouldReturnScoreWhenBothPlayersCheat(){
        Score score = machine.getScore(MoveType.CHEAT, MoveType.CHEAT);
        Assert.assertEquals(0,score.getPlayer1Score());
        Assert.assertEquals(0,score.getPlayer2Score());
    }

}

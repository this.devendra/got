import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class PlayerBehaviorTest {

    @Test
    public void shouldReturnMoveForConsolePlayer(){
        Scanner sc = new Scanner("1");
        PlayerBehavior playerBehavior = new ConsolePlayerBehavior(sc);
        assertEquals(MoveType.COOPERATE,playerBehavior.getMove());
    }

    @Test
    public void shouldReturnMoveForCoolPlayer(){
        PlayerBehavior playerBehavior = new CoolPlayerBehavior();
        assertEquals(MoveType.COOPERATE,playerBehavior.getMove());
    }

    @Test
    public void shouldReturnMoveForCheatPlayer(){
        PlayerBehavior playerBehavior = new CheatPlayerBehviour();
        assertEquals(MoveType.CHEAT,playerBehavior.getMove());
    }

}

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class GameTest {

    @Test
    public void shouldReturnScoreAtFirstRound() {
        Score expectedScore = new Score(2, 2);
        PrintStream printStream = Mockito.mock(PrintStream.class);
        Player mockPlayer1 = Mockito.mock(Player.class);
        Mockito.when(mockPlayer1.getMove()).thenReturn(MoveType.COOPERATE);
        Player mockPlayer2 = Mockito.mock(Player.class);
        Mockito.when(mockPlayer2.getMove()).thenReturn(MoveType.COOPERATE);
        Machine mockMachine = Mockito.mock(Machine.class);
        Mockito.when(mockMachine.getScore(MoveType.COOPERATE, MoveType.COOPERATE)).thenReturn(expectedScore);
        Game game = new Game(mockPlayer1, mockPlayer2, mockMachine, printStream);
        game.play(1);
        Mockito.verify(mockPlayer1, Mockito.times(1)).getMove();
        Mockito.verify(mockPlayer2, Mockito.times(1)).getMove();
        Mockito.verify(mockMachine, Mockito.times(1)).getScore(MoveType.COOPERATE, MoveType.COOPERATE);
        ArgumentCaptor<Score> captor = ArgumentCaptor.forClass(Score.class);
        Mockito.verify(printStream, Mockito.times(1)).println(captor.capture());
        Score actualScore = captor.getValue();
        assertEquals(expectedScore, actualScore);
    }


    @Test
    public void shouldReturnScoreAtTheEndOfTheGame() {
        Score expectedScore = new Score(2, 2);
        PrintStream printStream = Mockito.mock(PrintStream.class);
        Player mockPlayer1 = Mockito.mock(Player.class);
        Mockito.when(mockPlayer1.getMove()).thenReturn(MoveType.COOPERATE);
        Player mockPlayer2 = Mockito.mock(Player.class);
        Mockito.when(mockPlayer2.getMove()).thenReturn(MoveType.COOPERATE);
        Machine mockMachine = Mockito.mock(Machine.class);
        Mockito.when(mockMachine.getScore(MoveType.COOPERATE, MoveType.COOPERATE)).thenReturn(expectedScore);
        Game game = new Game(mockPlayer1, mockPlayer2, mockMachine, printStream);
        game.play(5);
        Mockito.verify(mockPlayer1, Mockito.times(5)).getMove();
        Mockito.verify(mockPlayer2, Mockito.times(5)).getMove();
        Mockito.verify(mockMachine, Mockito.times(5)).getScore(MoveType.COOPERATE, MoveType.COOPERATE);
        Mockito.verify(printStream, Mockito.times(5)).println(expectedScore);
    }
}

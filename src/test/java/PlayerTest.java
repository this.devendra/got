import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class PlayerTest {

    @Test
    public void shouldReturnCooperateMoveForPlayer(){
        Scanner sc = new Scanner("1");
        PlayerBehavior playerBehavior = new ConsolePlayerBehavior(sc);
        Player player = new Player(playerBehavior);
        MoveType moveType = player.getMove();
        assertEquals(MoveType.COOPERATE,moveType);
    }

    @Test
    public void shouldReturnCheatMoveForPlayer(){
        Scanner sc = new Scanner("2");
        PlayerBehavior playerBehavior = new ConsolePlayerBehavior(sc);
        Player player = new Player(playerBehavior);
        MoveType moveType = player.getMove();
        assertEquals(MoveType.CHEAT,moveType);
    }

    @Test
    public void shouldReturnInvalidMoveForPlayer(){
        Scanner sc = new Scanner("3");
        PlayerBehavior playerBehavior = new ConsolePlayerBehavior(sc);
        Player player = new Player(playerBehavior);
        MoveType moveType = player.getMove();
        assertEquals(null,moveType);
    }

    @Test
    public void shouldReturnTotalScore(){
        Scanner sc = new Scanner("1");
        PlayerBehavior playerBehavior = new ConsolePlayerBehavior(sc);
        Player player = new Player(playerBehavior);
        player.addScore(2);
        assertEquals(2,player.getTotalScore());
        player.addScore(1);
        assertEquals(3,player.getTotalScore());
    }

    @Test
    public void shouldReturnCoolPlayerMove(){
        PlayerBehavior playerBehavior = new CoolPlayerBehavior();
        Player player = new Player(playerBehavior);
        assertEquals(MoveType.COOPERATE,player.getMove());
    }

}

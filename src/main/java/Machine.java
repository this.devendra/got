import java.util.HashMap;

public class Machine {
    public Score getScore(MoveType player1Move,MoveType player2Move) {
        if(player1Move == MoveType.COOPERATE && player2Move == MoveType.COOPERATE){
            return new Score(2,2);
        }else if(player1Move == MoveType.CHEAT && player2Move == MoveType.COOPERATE){
            return new Score(3,-1);
        }else if(player1Move == MoveType.COOPERATE && player2Move == MoveType.CHEAT){
            return new Score(-1,3);
        }else{
            return new Score(0,0);
        }
    }
}

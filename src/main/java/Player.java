public class Player {

    private PlayerBehavior playerBehavior;
    private int totalScore = 0;

    public Player(PlayerBehavior playerBehavior){
        this.playerBehavior = playerBehavior;
    }

    public MoveType getMove() {
        return playerBehavior.getMove();
    }

    public void addScore(int score) {
        this.totalScore += score;
    }

    public int getTotalScore() {
        return this.totalScore;
    }

    public PlayerBehavior getPlayerBehavior() {
        return playerBehavior;
    }
}

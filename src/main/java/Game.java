import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Game extends Observable {

    private Player player1;
    private Player player2;
    private Machine machine;
    private PrintStream printStream;

    public Game(Player player1, Player player2, Machine machine, PrintStream printStream) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.printStream = printStream;
    }

    public void play(int rounds){
        MoveType player1Move;
        MoveType player2Move;
        for(int i=0; i<rounds; i++){
            player1Move = player1.getMove();
            player2Move = player2.getMove();
            Score score = this.machine.getScore(player1Move,player2Move);
            this.player1.addScore(score.getPlayer1Score());
            this.player2.addScore(score.getPlayer2Score());
            setChanged();
            notifyObservers(player1Move);
            printStream.println(score);
        }
    }

}

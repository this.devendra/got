public interface PlayerBehavior {
    MoveType getMove();
}

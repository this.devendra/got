public class CoolPlayerBehavior implements PlayerBehavior {

    @Override
    public MoveType getMove(){
        return MoveType.COOPERATE;
    }
}

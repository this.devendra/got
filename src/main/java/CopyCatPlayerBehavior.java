import java.util.Observable;
import java.util.Observer;

public class CopyCatPlayerBehavior implements PlayerBehavior, Observer {

    private MoveType copiedMove = MoveType.COOPERATE;
    @Override
    public void update(Observable observable, Object arg){
        System.out.println(arg);
        copiedMove = (MoveType) arg;
    }

    @Override
    public MoveType getMove(){
        System.out.println("Hola : "+copiedMove);
        return copiedMove;
    }

}

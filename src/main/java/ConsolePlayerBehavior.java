import java.util.Scanner;

public class ConsolePlayerBehavior implements PlayerBehavior {
    private Scanner input;

    public ConsolePlayerBehavior(Scanner input) {
        this.input = input;
    }

    @Override
    public MoveType getMove() {
        int userInput = input.nextInt();
        if(userInput == 1)
            return MoveType.COOPERATE;
        else if(userInput == 2)
            return MoveType.CHEAT;
        return null;
    }
}
